﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour {

    static public PathNode[] PathNodes;

    static bool pathGenerated = false;

    /// <summary>
    /// Returns the ground position at the exact spot where x,z coodrinates intersect with the terrain
    /// </summary>
    /// <param name="vec"></param>
    /// <returns></returns>
    public static Vector3 TerrainPosition(Vector3 vec)
    {
        RaycastHit hit;
        if (Physics.Raycast(vec + new Vector3(0, 500f, 0), -Vector3.up, out hit, 1000f, LayerMask.GetMask("Ground")))
        {
            return hit.point;
        }
        else
        {
            Debug.LogWarning("PathNode::TerrainPosition(" + vec + ") : Could not find intersecting terrain point from vector!");
            return vec;
        }
    }

    void Awake()
    {
        if (!pathGenerated)
        {
            Generate();
        }
    }

    public static void Generate()
    {
        // Find all pathnodes and cache them
        PathNodes = FindObjectsOfType<PathNode>();
        pathGenerated = true;
    }

}
