﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverInfo : MonoBehaviour {

    public CoverStrength Cover = CoverStrength.Low;

	public enum CoverStrength
    {
        None = 0,
        Low = 4,
        High = 6,
        Full = 10
    };

    public float CoverValue
    {
        get
        {
            return (int)Cover * 0.1f;
        }
    }

}
