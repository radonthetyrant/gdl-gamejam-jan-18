﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour {

	public static Game Instance;

	[Tooltip("Number of carottes visible at one time")]
	public int NumCarotts = 3;

	[Tooltip("How many minutes should the timer start at")]
	public int StartMinutes = 3;

	List<Carrot> carrotList = new List<Carrot>();

	public int Score {
		get
		{
			return _score;
		}
		set
		{
			_score = value;
			OnScoreChanged?.Invoke(value);
		}
	}
	int _score = 0;

    public int CarrotsEaten = 0;

	public float Timer
	{
		get
		{
			return _time;
		}
		set
		{
			_time = value;
			OnTimeChanged?.Invoke(value);
		}
	}
	float _time = 0;

	#region events

	public event Action<int> OnScoreChanged;

	public event Action<float> OnTimeChanged;

	#endregion

	public enum GameState
	{
		Running, TimeEnd, ScoreReached, Death
	}
	public GameState State;

	private void Awake()
	{
		Instance = this;

		// Setup physics
		Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Rabbit"), LayerMask.NameToLayer("Hunter"), true);
		Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Hunter"), LayerMask.NameToLayer("Cover"), true);
		Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Hunter"), LayerMask.NameToLayer("Solid"), true);
		Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Rabbit"), LayerMask.NameToLayer("Cover"), true);

		// Hide Mouse cursor
		Cursor.visible = false;
	}

	// Use this for initialization
	void Start () {
        PathNode.Generate();

		Score = 0;
		Timer = StartMinutes * 60;
		State = GameState.Running;
        CarrotsEaten = 0;

		System.Random rand = new System.Random();

		// Select carottes at random
		foreach (Carrot c in FindObjectsOfType<Carrot>())
		{
            c.gameObject.SetActive(false);
			carrotList.Add(c);
		}
		int n = carrotList.Count;
		while (n > 1)
		{
			n--;
			int k = rand.Next(n + 1);
			Carrot c = carrotList[k];
			carrotList[k] = carrotList[n];
			carrotList[n] = c;
		}

        if (NumCarotts >= carrotList.Count)
        {
            Debug.LogError("Not enough carrots in the gameworld!");
            return;
        }

        for (int i = 0; i < NumCarotts; i++)
        {
            carrotList[i].gameObject.SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () {
		switch (State)
		{
            case GameState.Running:
                Timer -= Time.deltaTime;
			    if (Timer <= 0)
			    {
				    Timer = 0;
				    EndGame(GameState.TimeEnd);
			    }
                break;

            case GameState.Death:
            case GameState.ScoreReached:
            case GameState.TimeEnd:
                // key presses
                if (Input.GetKeyUp(KeyCode.KeypadEnter) || Input.GetKeyUp(KeyCode.Return))
                {
                    // Return to main menu
                    SceneManager.LoadScene(0);
                }
                else if (Input.GetKeyUp(KeyCode.Escape))
                {
#if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
                }
                break;
		}
	}

	public void EndGame(GameState reason)
	{
		Debug.Log("Game::EndGame(" + reason.ToString() + ") GAME END DETECTED");
        State = reason;

        // Disable all Hunters
        foreach (HunterController h in FindObjectsOfType<HunterController>())
        {
            h.Freeze();
        }
        // Despawn all carrots
        foreach (Carrot c in carrotList)
        {
            c.gameObject.SetActive(false);
        }
        GameUiController.Instance.ShowEndScreen(reason);
	}

	/// <summary>
	/// Removes one carrot from the game and spawns a new one
	/// </summary>
	public void ConsumeCarrot(Carrot carrot, int carrotScore)
	{
		Score += carrotScore;
        CarrotsEaten++;
		carrot.gameObject.SetActive(false);

		// Sanity check: to not run into an infinite loop here if we have not enough carrots
		if (carrotList.Count < 3)
		{
			Debug.LogWarning("Not enough carrots exist in the game!");
			return;
		}

		// Find a new carrot that isnt this one and activate it
		Carrot newCarrot = carrot;
		do
		{
			int k = UnityEngine.Random.Range(0, carrotList.Count - 1);
			newCarrot = carrotList[k];
		}
		while (newCarrot == carrot && !newCarrot.gameObject.activeSelf);

		// Found a new carrot!
		newCarrot.gameObject.SetActive(true);

        // Startle random hunter that is observing or patrolling
        List<HunterController> list = new List<HunterController>();
        foreach (HunterController h in FindObjectsOfType<HunterController>())
        {
            if (h.State == HunterController.HunterState.Observing || h.State == HunterController.HunterState.Patrolling)
            {
                list.Add(h);
            }
        }
        if (list.Count > 0)
        {
            int k = UnityEngine.Random.Range(0, list.Count - 1);
            list[k].InvestigateCurrentRabbitPosition();
        }
	}


}
