﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HunterController : MonoBehaviour {

    [SerializeField]
    Transform EyePos;

    [SerializeField]
    AudioSource AudioAlert;

    [SerializeField]
    AudioSource AudioShot;

    NavMeshAgent Agent;
    RabbitController Rabbit;

    [SerializeField]
    GameObject[] HunterKeyframes;

    public float ObservingTimeMin = 2;
    public float ObservingTimeMax = 6;

    public float RecognitionAngle = 120f;
    public float RecognitionTimer = 1f;
    public float RecognitionRange = 20f;

    public float AimingRange = 10f;
    public float AimingTime = 2f;

    public float HuntStopDistance = 5f;
    public float RegularSpeed = 1.5f;
    public float HuntingSpeed = 2.5f;

    public GameObject[] VisibilityIndicators;

    #region events

    public event Action<HunterState> OnStateChanged;

    #endregion
    
    float observingTimeLeft;
    float recognitionTimeLeft;
    bool wasVisiblePreviously = false;
    bool hunterIsInvestigating = false;
    bool rabbitIsVisible = false;
    bool isAiming = false;
    float lastLOSUpdate;
    float defaultStoppingDistance;
    float aimingTimeLeft;

    public HunterState State
    {
        get
        {
            return _currentState;
        }
        set
        {
            _currentState = value;
            OnStateChanged(value);
        }
    }
    [SerializeField]
    HunterState _currentState = HunterState.Invalid;

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        Rabbit = RabbitController.Instance;
        OnStateChanged += HunterController_OnStateChanged;
        defaultStoppingDistance = Agent.stoppingDistance;
    }

    // Use this for initialization
    void Start()
    {
        // Walk to random Pathnode
        MoveToPathNode();
        lastLOSUpdate = Time.time;

        SelectSpawnPoint();
        State = HunterState.Spawned;
    }

    // Update is called once per frame
    void Update()
    {
        switch (State)
        {
            case HunterState.Observing:
                if (observingTimeLeft <= 0)
                {
                    SetVisibleIndicator(0);
                    MoveToPathNode();
                }
                else
                {
                    observingTimeLeft -= Time.deltaTime;
                }
                break;

            case HunterState.Patrolling:

                // Cycle keyframes
                int kf = Mathf.FloorToInt(Time.time * 1f) % 2;
                SetKeyframe(kf);

                if (Agent.remainingDistance <= .1f)
                {
                    Debug.Log("HunterController::Update() : Patrolling path completed, start observing");
                    State = HunterState.Observing;
                }
                break;

            case HunterState.Attacking:

                // Cycle keyframes
                int kfx = Mathf.FloorToInt(Time.time * 1f) % 2;
                SetKeyframe(kfx);

                // if hunter is in range, start aiming
                if (Vector3.Distance(Rabbit.transform.position, transform.position) <= AimingRange)
                {
                    State = HunterState.Aiming;
                }
                else
                {
                    Debug.Log("HunterController::Update() : Attacking, but rabbit not yet in range... want: " + AimingRange + ", is: " + Vector3.Distance(Rabbit.transform.position, transform.position));
                }

                break;

            case HunterState.Aiming:

                if (!rabbitIsVisible)
                {
                    Debug.Log("HunterController::Update() : Was aiming for rabbit, but lost vision. checking out last location...");
                    isAiming = false;
                    SetVisibleIndicator(2);
                    MoveToRabbit();
                }
                else
                {
                    // Still aiming
                    aimingTimeLeft -= Time.deltaTime;
                    if (aimingTimeLeft <= 0)
                    {
                        // Shoot!
                        ShootRabbit();
                        isAiming = false;
                        State = HunterState.Attacking;
                    }
                    else
                    {
                        transform.LookAt(Rabbit.transform.position);
                        //transform.localRotation = Quaternion.Euler(0, transform.localRotation.y, 0);
                    }
                }

                break;

        }

        if (tickDivider % 8 == 0)
            UpdateLineOfSight();

        ++tickDivider;
    }
    int tickDivider = 0;

    private void SelectSpawnPoint()
    {
        // Warp to random pathnode that isnt within 25 units of the player
        float dist = 9999;
        PathNode p;
        int tries = 0;
        do
        {
            int k = UnityEngine.Random.Range(0, PathNode.PathNodes.Length - 1);
            p = PathNode.PathNodes[k];
            dist = Vector3.Distance(p.transform.position, Rabbit.transform.position);
        }
        while (dist < 25f && ++tries < 5);
        Debug.Log("HunterController::SelectSpawnPoint() : Spawn selection " + (p != null ? "succeeded" : "FAILED") + " after " + tries + " tries");
        if (p != null)
        {
            //Agent.Warp(PathNode.TerrainPosition(p.transform.position));
            transform.position = PathNode.TerrainPosition(p.transform.position);
        }
    }

    private void ShootRabbit()
    {
        AudioShot.Play();
        Rabbit.Shoot();
    }

    void SetKeyframe(int f)
    {
        f = f % HunterKeyframes.Length;
        for (int i = 0; i < HunterKeyframes.Length; i++)
        {
            foreach (Renderer r in HunterKeyframes[i].GetComponentsInChildren<Renderer>())
            {
                r.enabled = (i == f);
            }
        }
    }

    private void HunterController_OnStateChanged(HunterState state)
    {
        switch (state)
        {
            case HunterState.Observing:

                SetKeyframe(0);

                // Halt the agent
                Agent.isStopped = true;
                Agent.stoppingDistance = defaultStoppingDistance;
                Agent.speed = RegularSpeed;
                // Start observing timer
                observingTimeLeft = UnityEngine.Random.Range(ObservingTimeMin, ObservingTimeMax); // Wait between Min and Max seconds
                // If hunter is investigating last known position, make this a little more longer
                if (hunterIsInvestigating)
                    observingTimeLeft *= 1.5f;
                hunterIsInvestigating = false;

                Debug.Log("HunterController::HunterController_OnStateChanged("+state.ToString()+") : Starting to observe, timer set to "+observingTimeLeft+" Seconds");
                break;

            case HunterState.Patrolling:

                SetKeyframe(1);

                // Resume the agent
                Agent.isStopped = false;
                Agent.stoppingDistance = defaultStoppingDistance;
                Agent.speed = RegularSpeed;
                break;

            case HunterState.Attacking:
                // Resume the agent
                Agent.isStopped = false;
                Agent.stoppingDistance = HuntStopDistance;
                Agent.speed = HuntingSpeed;
                break;

            case HunterState.Spawned:
                Agent.isStopped = false;
                Agent.stoppingDistance = defaultStoppingDistance;
                Agent.speed = RegularSpeed;

                // First order of business: Observe a bit
                State = HunterState.Observing;
                break;

            case HunterState.Aiming:

                SetKeyframe(2);

                Agent.isStopped = true;
                Agent.stoppingDistance = defaultStoppingDistance;
                Agent.speed = HuntingSpeed;
                isAiming = true;
                aimingTimeLeft = AimingTime;
                break;

        }
    }

    private void MoveToPathNode()
    {
        if (PathNode.PathNodes == null || PathNode.PathNodes.Length < 1)
        {
            Debug.LogError("HunterController::MoveToPathNode() : No pathnodes found!");
            return;
        }

        SortedList list = new SortedList();
        foreach (PathNode p in PathNode.PathNodes)
        {
            float key = Vector3.Distance(transform.position, p.transform.position);
            list.Add(key, p);
        }

        if (UnityEngine.Random.Range(0, 2) > 1)
        {
            // Select random of 4 nearest pathnodes
            int index = UnityEngine.Random.Range(1, 5);
            if (index < list.Count)
                Agent.SetDestination(PathNode.TerrainPosition((list.GetByIndex(index) as PathNode).transform.position));
            else
                Agent.SetDestination(PathNode.TerrainPosition((list.GetByIndex(0) as PathNode).transform.position));
        }
        else
        {
            // Select random of 5 furthest pathnodes
            int index = UnityEngine.Random.Range(list.Count - 6, list.Count - 1);
            index = Mathf.Clamp(index, 0, list.Count - 1);
            Agent.SetDestination(PathNode.TerrainPosition((list.GetByIndex(index) as PathNode).transform.position));
        }

        State = HunterState.Patrolling;
    }

    void MoveToRabbit()
    {
        Agent.SetDestination(PathNode.TerrainPosition(Rabbit.transform.position));
        State = HunterState.Patrolling;
    }

    void UpdateLineOfSight()
    {
        Ray ray = new Ray(EyePos.transform.position, Rabbit.VisbleTip.position - EyePos.transform.position);

        float directDist = Vector3.Distance(EyePos.transform.position, Rabbit.transform.position);
        float directAngle = Vector3.Angle(transform.forward, ray.direction);

        bool isInViewAngle = directAngle < RecognitionAngle * .5f
            && directDist <= RecognitionRange;

        if (isInViewAngle)
        {
            RaycastHit[] hitInfo = Physics.RaycastAll(ray, 100f, LayerMask.GetMask("Ground", "Cover", "Solid"));

            SortedList list = new SortedList();
            foreach (RaycastHit hit in hitInfo)
            {
                if (hit.distance > directDist || hit.transform == null)
                    continue; // Ignore what lies beyond the rabbit, only in between counts

                list.Add(hit.distance, hit.transform);
            }

            bool isVisible = false;
            if (list.Count > 0)
            {
                Debug.DrawRay((list.GetByIndex(0) as Transform).position, Vector3.up, Color.magenta, .2f);
                //Debug.Log("Nearest cover: "+ (list.GetByIndex(0) as Transform).name);
                float visibleValue = DetermineVisibility(list.Values, directDist, directAngle);
                isVisible = visibleValue > 0;

                UpdateVisualIndicator(isVisible, visibleValue, Rabbit.transform.position);
            }
            else
            {
                // No cover items hit, completely visible
                isVisible = true;
                UpdateVisualIndicator(true, 1, Rabbit.transform.position);
            }

            Debug.DrawRay(EyePos.transform.position, Rabbit.VisbleTip.position - EyePos.transform.position, (isInViewAngle && isVisible) ? Color.yellow : new Color(0.66f, 0.66f, 0.66f), .2f);
        }
        else
        {
            Debug.DrawRay(EyePos.transform.position, Rabbit.VisbleTip.position - EyePos.transform.position, new Color(0.33f, 0.33f, 0.33f), .2f);
            UpdateVisualIndicator(false, 0, Rabbit.transform.position);
        }

        // Try some unstuck logic
        if (Agent.pathStatus == NavMeshPathStatus.PathInvalid && ++failedPathCounter >= 30)
        {
            Debug.LogWarning("HunterController::UpdateLineOfSight() : Hunter seems to be stuck. Unstucking...");
            // Reset aggro state
            hunterIsInvestigating = false;
            SetVisibleIndicator(0);
            // Move somewhere random
            MoveToPathNode();

            failedPathCounter = 0;
        }
        else if (Agent.pathStatus != NavMeshPathStatus.PathInvalid)
        {
            failedPathCounter = 0;
        }

        lastLOSUpdate = Time.time;
    }
    int failedPathCounter = 0;

    private void UpdateVisualIndicator(bool isVisible, float visibleValue, Vector3 lastKnownPosition)
    {
        // Was visible and is still visible
        if (wasVisiblePreviously && isVisible)
        {
            // Ignore the attack logic when we're amining
            if (State != HunterState.Aiming)
            {
                // Decrease timer
                recognitionTimeLeft -= (Time.time - lastLOSUpdate);
                if (recognitionTimeLeft < 0)
                {
                    if (State != HunterState.Attacking)
                    {
                        // Not yet attacking, start!
                        Debug.Log("HunterController::UpdateVisualIndicator(" + isVisible + ", " + visibleValue + ") : Rabbit is now recognized");
                        SetVisibleIndicator(3);
                        StartAttacking();
                    }
                    else
                    {
                        // Still attacking, update position
                        UpdateAttacking();
                    }
                }
            }
        }
        // Was visible but lost line of sight
        else if (wasVisiblePreviously && !isVisible)
        {
            // Show questionmark and direct the hunter to walk to the last known position
            Debug.Log("HunterController::UpdateVisualIndicator(" + isVisible + ", " + visibleValue + ") : visuals lost, checking out last known position");
            InvestigateCurrentRabbitPosition();
        }
        // Rabbit just became visible
        else if (!wasVisiblePreviously && isVisible)
        {
            // Reset timer
            recognitionTimeLeft = RecognitionTimer;
            SetVisibleIndicator(1);
        }
        // Not visible, wasnt visible before
        else if (!hunterIsInvestigating)
        {
            // Hide the indicator alltogether
            SetVisibleIndicator(0);
        }

        wasVisiblePreviously = isVisible;
        rabbitIsVisible = isVisible;
    }

    public void InvestigateCurrentRabbitPosition()
    {
        SetVisibleIndicator(2);
        hunterIsInvestigating = true;
        MoveToRabbit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="visibleLevel">0: invisible, 1: suspicious, 2: slightly visible, 3: recognized</param>
    private void SetVisibleIndicator(int visibleLevel)
    {
        switch (visibleLevel)
        {
            case 0:
                VisibilityIndicators[0].SetActive(false);
                VisibilityIndicators[1].SetActive(false);
                VisibilityIndicators[2].SetActive(false);
                break;
            case 1:
                VisibilityIndicators[0].SetActive(true);
                VisibilityIndicators[1].SetActive(false);
                VisibilityIndicators[2].SetActive(false);
                break;
            case 2:
                VisibilityIndicators[0].SetActive(false);
                VisibilityIndicators[1].SetActive(true);
                VisibilityIndicators[2].SetActive(false);
                break;
            case 3:
                VisibilityIndicators[0].SetActive(false);
                VisibilityIndicators[1].SetActive(false);
                VisibilityIndicators[2].SetActive(true);
                break;
        }

    }

    private void StartAttacking()
    {
        Debug.Log("HunterController::StartAttacking() : Starting to attack");
        State = HunterState.Attacking;
        if (!AudioAlert.isPlaying)
            AudioAlert.Play();
    }

    void UpdateAttacking()
    {
        Agent.SetDestination(PathNode.TerrainPosition(Rabbit.transform.position));
    }

    private float DetermineVisibility(ICollection coverList, float rabbitDistance, float rabbitAngle)
    {
        float baseVisibility = 1f;

        foreach (object o in coverList)
        {
            Transform t = o as Transform;
            if (t == null)
            {
                Debug.LogError("Failed to cast object as transform, object is a " + o.GetType().ToString());
                return 0;
            }

            // Try to get coverinfo. if the component doesnt exist, cinfo will be null
            CoverInfo cinfo = t.GetComponent<CoverInfo>();

            if (t.gameObject.layer == LayerMask.NameToLayer("Solid"))
                return 0; // If there is a solid inbetween, the visibility is zero

            if (t.gameObject.layer == LayerMask.NameToLayer("Cover"))
            {
                float rageModifier = rabbitIsVisible ? 0.5f : 1f;
                baseVisibility -= (rageModifier * (cinfo != null ? cinfo.CoverValue : 0.3f));
            }
        }

        return Mathf.Clamp(baseVisibility, 0, 1f);
    }

    public void Freeze()
    {
        Agent.isStopped = true;
        Agent.enabled = false;
        enabled = false;
    }

    public enum HunterState
    {
        Invalid = 0,
        Spawned,
        Observing, Patrolling, Attacking, Aiming
    }

}

