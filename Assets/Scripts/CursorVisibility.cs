﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorVisibility : MonoBehaviour {

    private void Awake()
    {
        Cursor.visible = true;
    }
}
