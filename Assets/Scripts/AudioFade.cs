﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioFade : MonoBehaviour {

    AudioSource Source;

    float targetSoundLevel;

    public float VolumeRate = 1f;

	// Use this for initialization
	void Start () {
        Source = GetComponent<AudioSource>();
        targetSoundLevel = Source.volume;
        Source.volume = 0;
	}
	
	// Update is called once per frame
	void Update () {
        Source.volume += Time.deltaTime * VolumeRate;
        if (Source.volume >= targetSoundLevel)
        {
            Source.volume = targetSoundLevel;
            enabled = false;
        }
	}
}
