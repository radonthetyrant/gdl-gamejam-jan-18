﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUiController : MonoBehaviour {

    [SerializeField]
    Text ScoreLabel;

    [SerializeField]
    Text TimeLabel;

    [SerializeField]
    GameObject Panel_Win;

    [SerializeField]
    GameObject Panel_Lose;

    [SerializeField]
    Text GameWinText;

    [SerializeField]
    Image[] HealthIcons;

    public static GameUiController Instance;

    private void Awake()
    {
        Instance = this;
        Game.Instance.OnScoreChanged += Instance_OnScoreChanged;
        Game.Instance.OnTimeChanged += Instance_OnTimeChanged;
        RabbitController.Instance.OnHealthChanged += Instance_OnHealthChanged;
    }

    private void Instance_OnHealthChanged(int health)
    {
        for (int i = 0; i < HealthIcons.Length; i++)
        {
            HealthIcons[i].gameObject.SetActive(health > i);
        }
    }

    private void Instance_OnTimeChanged(float time)
    {
        int seconds = Mathf.FloorToInt(time) % 60;
        int minutes = Mathf.FloorToInt(time / 60f);
        int tenthSeconds = Mathf.FloorToInt(time * 10f) % 10;

        TimeLabel.text = string.Format("TIME: {0}:{1}:{2}", minutes.ToString("00"), seconds.ToString("00"), tenthSeconds.ToString("0"));

        if (time < 30f)
            TimeLabel.color = Color.red;
    }

    private void Instance_OnScoreChanged(int score)
    {
        ScoreLabel.text = "SCORE: "+score.ToString("000000");
    }

    public void ShowEndScreen(Game.GameState reason)
    {
        switch (reason)
        {
            case Game.GameState.Death:
                Panel_Lose.SetActive(true);
                break;

            case Game.GameState.ScoreReached:
            case Game.GameState.TimeEnd:
                GameWinText.text = string.Format(GameWinText.text, Game.Instance.CarrotsEaten);
                Panel_Win.SetActive(true);
                break;

            default:
                Panel_Win.SetActive(false);
                Panel_Lose.SetActive(false);
                break;
        }
    }
}
