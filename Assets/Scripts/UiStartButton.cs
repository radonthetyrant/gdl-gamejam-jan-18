﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UiStartButton : MonoBehaviour {

    public Color RegularColor;

    public Color HoverColor;

    public Color ClickColor;

    TextMesh TextMesh;

    void Awake()
    {
        TextMesh = GetComponent<TextMesh>();
    }

    void Start()
    {
        TextMesh.color = RegularColor;
    }

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000f, LayerMask.GetMask("Solid")))
        {
            if (!hover)
            {
                hover = true;
                OnPointerEnter();
            }
            else if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Return))
            {
                OnPointerClick();
            }
        }
        else if (hover)
        {
            hover = false;
            OnPointerExit();
        }
    }
    bool hover = false;

    public void OnPointerClick()
    {
        TextMesh.color = ClickColor;

        SceneManager.LoadScene(1);
    }

    public void OnPointerEnter()
    {
        TextMesh.color = HoverColor;
    }

    public void OnPointerExit()
    {
        TextMesh.color = RegularColor;
    }

}
