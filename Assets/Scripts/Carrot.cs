﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carrot : MonoBehaviour {

    public int AddScore = 100;

    [SerializeField]
    AudioSource EatSound;

    private void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("Rabbit"))
            return;

        Debug.Log("Carrot::OnTriggerEnter("+other.name+") : NOM!");
        RabbitController.Instance.PlayEatSound();
        Game.Instance.ConsumeCarrot(this, AddScore);
    }

}
