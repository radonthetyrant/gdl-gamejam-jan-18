﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RabbitController : MonoBehaviour {

    public static RabbitController Instance;

    [SerializeField]
    Transform CamSpot;

    [SerializeField]
    AudioSource AudioHit;

    [SerializeField]
    AudioSource AudioEat;

    [SerializeField]
    GameObject[] RabbitKeyframes;

    public Transform VisbleTip;

    public float MovementRate = 1f;
    public float AirMovementRate = .1f;
    public float KeyTurnRate = 1f;

    public float JumpForce = 1f;
    public float JumpCooldownSeconds = 1f;
    public float MouseAxisXSensitivity = 1f;
    public float MouseAxisYSensitivity = 1f;

    public int StartingHealth = 10;

    public Vector3 RaycastOffset = Vector3.up;

    float _jumpCooldown = 0f;
    Vector3 _lastPosition;

    #region cache

    Rigidbody Rigidbody;

    #endregion

    #region events

    public event Action<int> OnHealthChanged;

    #endregion

    public bool IsJumping = false;

    public int Health
    {
        get
        {
            return _health;
        }
        set
        {
            _health = value;
            OnHealthChanged?.Invoke(value);
        }
    }
    int _health;

    private void Awake()
    {
        Instance = this; // There can be only one
        Rigidbody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start()
    {
        // Attach camera to self
        Camera.main.transform.parent = CamSpot;
        // Reset camera
        Camera.main.transform.localPosition = Vector3.zero;
        Camera.main.transform.localRotation = Quaternion.Euler(Vector3.zero);

        Health = StartingHealth;

        SetKeyframe(0);
        _lastPosition = transform.position;

        // Spawn at a random pathnode
        PathNode p;
        int k = UnityEngine.Random.Range(0, PathNode.PathNodes.Length - 1);
        p = PathNode.PathNodes[k];
        transform.position = p.transform.position;
    }

    void FixedUpdate()
    {
        if (Input.anyKey || Input.anyKeyDown)
            HandleInput();
    }

    // Update is called once per frame
    void Update () {

        float moveDist = Vector3.Distance(_lastPosition, transform.position);

        if (IsJumping && moveDist > 0.01f)
        {
            SetKeyframe(1);
            CheckLanding();
        }
        else if (moveDist > 0.01f)
        {
            // Cycle keyframes
            int kf = Mathf.FloorToInt(Time.time * 2.5f);
            SetKeyframe(kf);

        }
        else if (moveDist <= 0.01f)
        {
            SetKeyframe(0);
            if (IsJumping)
                EndJump();
        }
        _lastPosition = transform.position;

        HandleCameraRotation();

        if (_jumpCooldown >= 0)
            _jumpCooldown -= Time.deltaTime;

    }

    public void PlayEatSound()
    {
        AudioEat.Play();
    }

    void SetKeyframe(int f)
    {
        f = f % RabbitKeyframes.Length;
        for (int i = 0; i < RabbitKeyframes.Length; i++)
        {
            RabbitKeyframes[i].GetComponent<Renderer>().enabled = (i == f);
        }
    }

    public void Shoot()
    {
        if (UnityEngine.Random.Range(0, 2) >= 1) // Add a bit of randomness, the hunter may miss
        {
            AudioHit.Play();
            Health--;
            if (Health <= 0)
            {
                Game.Instance.EndGame(Game.GameState.Death);
                // Hide everything
                foreach (Renderer r in GetComponentsInChildren<Renderer>())
                {
                    r.enabled = false;
                }
                enabled = false; // disable self to disable input
            }
        }
    }

    /// <summary>
    /// Checks if we have contact with the ground after the vertical velocity became negative
    /// </summary>
    private void CheckLanding()
    {
        // Velocity <= 0 => falling or stationary
        RaycastHit groundHitInfo;
        bool groundHit = Physics.Raycast(transform.position + RaycastOffset, -Vector3.up, out groundHitInfo, 10f, 1 << 8);
        if (Rigidbody.velocity.y <= 0 && groundHit && Vector2.Distance((transform.position - RaycastOffset), groundHitInfo.point) < .1f)
        {
            Debug.Log("RabbitController::CheckLanding() : End of Jump detected");
            EndJump();
        }
        else
        {
            //Debug.Log("RabbitController::CheckLanding() : velocity: " + _rigidbody.velocity.y + ", hasGround: "+groundHit+" dist: " +Vector2.Distance((transform.position - RaycastOffset), groundHitInfo.point));
        }
    }

    private void EndJump()
    {
        IsJumping = false;
        _jumpCooldown += JumpCooldownSeconds;
    }

    private void HandleInput()
    {
        if (Input.GetKey(KeyCode.W))
        {
            // Forward
            transform.position = transform.position + (transform.forward * (IsJumping ? AirMovementRate : MovementRate) * Time.deltaTime);
            Debug.DrawRay(transform.position, transform.forward * 5, Color.red);

        }
        if (Input.GetKey(KeyCode.S))
        {
            // Backward
            transform.position = transform.position + (-transform.forward * (IsJumping ? AirMovementRate : MovementRate) * Time.deltaTime);
            Debug.DrawRay(transform.position, -transform.forward * 5, Color.blue);
        }
        if (Input.GetKey(KeyCode.A))
        {
            // Forward
            transform.position = transform.position + (-transform.right * (IsJumping ? AirMovementRate : MovementRate) * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            // Backward
            transform.position = transform.position + (transform.right * (IsJumping ? AirMovementRate : MovementRate) * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space) && !IsJumping)
        {
            // Only jump when we have ground contact
            Jump();
        }
    }

    void HandleCameraRotation()
    {
        transform.RotateAround(transform.position, Vector3.up, Time.deltaTime * Input.GetAxis("Mouse X") * MouseAxisXSensitivity);
        Camera.main.transform.RotateAround(transform.position, transform.right, Time.deltaTime * Input.GetAxis("Mouse Y") * -MouseAxisYSensitivity);
        //Camera.main.transform.localRotation = Quaternion.Euler(Mathf.Min(40, Mathf.Max(-30, Camera.main.transform.localRotation.x)), 0, 0);
    }

    private void Jump()
    {
        if (_jumpCooldown >= 0)
        {
            Debug.Log("RabbitController::Jump() : Still on cooldown: " + _jumpCooldown);
            return;
        }
        Debug.Log("RabbitController::Jump() : Jump Started");

        Rigidbody.AddForce(transform.up * JumpForce, ForceMode.Impulse);
        IsJumping = true;
    }

}
